#ifndef CALIBRATION_H
#define CALIBRATION_H
#include <stdint.h>

#define GENERIC_CAL 0
#define THROTTLE_OUT_CAL 1
#define LOAD_OUT_CAL 2


uint16_t get_calibrated_value(uint16_t value, uint8_t calibration_setting);


#endif // CALIBRATION_H
