#ifndef SENSOR_CONF_H
#define SENSOR_CONF_H
#include <stdint.h>
#include <avr/io.h>
#include <stdbool.h>
//GPIO
/*
const  uint8_t GPIO0_BIT  = PB2;
const  uint8_t* GPIO0_PIN  = &PINB;
const  uint8_t* GPIO0_PORT = &PORTB;
const  uint8_t* GPIO0_DDR  = &DDRB;

const  uint8_t GPIO1_BIT  = PD7;
const  uint8_t* GPIO1_PIN  = &PIND;
const  uint8_t* GPIO1_PORT = &PORTD;
const  uint8_t* GPIO1_DDR  = &DDRD;

const  uint8_t GPIO2_BIT  = PC0;
const  uint8_t* GPIO2_PIN  = &PINC;
const  uint8_t* GPIO2_PORT = &PORTC;
const  uint8_t* GPIO2_DDR  = &DDRC;

const  uint8_t GPIO3_BIT  = PB7;
const  uint8_t* GPIO3_PIN  = &PINB;
const  uint8_t* GPIO3_PORT = &PORTB;
const  uint8_t* GPIO3_DDR  = &DDRB;

const  uint8_t GPIO4_BIT  = PB1;
const  uint8_t* GPIO4_PIN  = &PINB;
const  uint8_t* GPIO4_PORT = &PORTB;
const  uint8_t* GPIO4_DDR  = &DDRB;

const  uint8_t GPIO5_BIT  = PB0;
const  uint8_t* GPIO5_PIN  = &PINB;
const  uint8_t* GPIO5_PORT = &PORTB;
const  uint8_t* GPIO5_DDR  = &DDRB;
*/

#define SSR0 0
#define SSR1 1
#define SSR2 2

struct in_config
{
	uint8_t en : 1;
	uint8_t slot : 2;
	uint8_t frame: 3;
	uint16_t (*get_value)(void);
};

struct out_config
{
	uint8_t en : 1;
	uint8_t slot : 2;
	uint8_t frame: 3;
	void (*set_value)(uint8_t);
};

struct bit_config
{
	uint8_t en : 1;
	uint8_t ddr : 1;
	uint8_t bit : 4;
	uint8_t slot : 2;
	uint8_t frame: 3;
	uint8_t *pin;
	uint8_t *port;
	//uint8_t *ddr;
	void (*set_value)(uint8_t);
	uint8_t (*get_value)(void);
};

struct sys_config
{
	struct in_config adc_1;
	struct in_config adc_2;
	struct in_config adc_3;
	struct in_config adc_4;
	struct in_config amp_0;
	struct in_config amp_1;
	//struct in_config amp_1;

	struct out_config d2a;
	struct out_config pwm_0;
	struct out_config pwm_1;
	struct out_config ssr_0;
	struct out_config ssr_1;
	struct out_config ssr_2;

	struct bit_config gpio0;
	struct bit_config gpio1;
	struct bit_config gpio2;
	struct bit_config gpio3;
	struct bit_config gpio4;
	struct bit_config gpio5;
};

//Check for state of SSR
bool ssr_is_closed(uint8_t ssr);

//Set state of SSR
void set_ssr(uint8_t ssr, bool close);

//Set direction of GPIO
void set_gpio_direction(uint8_t gpio_num, bool in);

//Check for direction of GPIO
bool gpio_is_input(uint8_t gpio_num);

//Set value of GPIO
void set_gpio_value(uint8_t gpio_num, bool hi);

//Get value/state of GPIO
bool get_gpio_value(uint8_t gpio_num);

//Setup pin directions, etc
void setup_io(struct sys_config *sys);


#endif

