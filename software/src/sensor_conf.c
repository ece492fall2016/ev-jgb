#include "sensor_conf.h"

#include <avr/eeprom.h>

void setup_io(struct sys_config *sys)
{
//	DDRA = 0;
	DDRB = 0;
	DDRC = 0;
	DDRD = 0;

	//ADC1
	sys->adc_1.en = 0;
	//sys->adc_1.ddr = 0;
	sys->adc_1.slot = 0;
	sys->adc_1.frame = 0;
	sys->adc_1.get_value = 0;

	//ADC2
	sys->adc_2.en = 0;
	//sys->adc_2.ddr = 0;
	sys->adc_2.slot = 0;
	sys->adc_2.frame = 0;
	sys->adc_1.get_value = 0;

	//ADC3
	sys->adc_3.en = 0;
	//sys->adc_3.ddr = 0;
	sys->adc_3.slot = 0;
	sys->adc_3.frame = 0;
	sys->adc_1.get_value = 0;

	//ADC4
	sys->adc_4.en = 0;
	//sys->adc_4.ddr = 0;
	sys->adc_4.slot = 0;
	sys->adc_4.frame = 0;
	sys->adc_1.get_value = 0;

	//AMP0
	sys->amp_0.en = 0;
	//sys->amp_0.ddr = 0;
	sys->amp_0.slot = 0;
	sys->amp_0.frame = 0;

	//AMP1
	sys->amp_1.en = 0;
	//sys->amp_1.ddr = 0;
	sys->amp_1.slot = 0;
	sys->amp_1.frame = 0;

	//D2A
	sys->d2a.en = 0;
	//sys->d2a.ddr = 0;
	sys->d2a.slot = 0;
	sys->d2a.frame = 0;
	DDRC |= (1 << PC7);

	//PWM0
	sys->pwm_0.en = 0;
	//sys->pwm_0.ddr = 0;
	sys->pwm_0.slot = 0;
	sys->pwm_0.frame = 0;
	DDRC |= (1 << PC1);

	//PWM1
	sys->pwm_1.en = 0;
	//sys->pwm_1.ddr = 0;
	sys->pwm_1.slot = 0;
	sys->pwm_1.frame = 0;
	DDRD |= (1 << PD2);

	//SSR0
	sys->ssr_0.en = 0;
	//sys->ssr_0.ddr = 0;
	sys->ssr_0.slot = 0;
	sys->ssr_0.frame = 0;
	DDRC |= (1 << PC6);

	//SSR1
	sys->ssr_1.en = 0;
	//sys->ssr_1.ddr = 0;
	sys->ssr_1.slot = 0;
	sys->ssr_1.frame = 0;
	DDRD |= (1 << PD0);

	//SSR2
	sys->ssr_2.en = 0;
	//sys->ssr_2.ddr = 0;
	sys->ssr_2.slot = 0;
	sys->ssr_2.frame = 0;
	DDRD |= (1 << PD1);
}

bool ssr_is_closed(uint8_t ssr)
{
    switch(ssr)
    {
        case SSR0:  return (((DDRC & (1 << PC6)) & (PORTC & (1 << PC6))) != 0);
            break;
        case SSR1:  return (((DDRD & (1 << PD0)) & (PORTD & (1 << PD0))) != 0);
            break;
        case SSR2:  return (((DDRD & (1 << PD1)) & (PORTD & (1 << PD1))) != 0);
            break;
    }
    return false;
}

void set_ssr(uint8_t ssr, bool close)
{
    switch(ssr)
    {
        case SSR0:
            if(close)
            {
                DDRC |= (1 << PC6); //0
                PORTC |= (1 << PC6);
            }
            else
            {
                PORTC &= ~(1 << PC6);
            }
            break;
        case SSR1:
            if(close)
            {
                DDRD |= (1 << PD0);//1
                PORTD |= (1 << PD0);
            }
            else
            {
                PORTD &= ~(1 << PD0);
            }
            break;
        case SSR2:
            if(close)
            {
                DDRD |= (1 << PD1);//2
                PORTD |= (1 << PD1);
            }
            else
            {
                PORTD &= ~(1 << PD1);
            }
            break;
    }
}

void set_gpio_direction(uint8_t gpio_num, bool in)
{
    switch(gpio_num)
    {
        case 0:
            if(in)
            {
                DDRB &= ~(1 << PB2);
            }
            else
            {
                DDRB |= (1 << PB2);
            }
            break;
        case 1:
            if(in)
            {
                DDRD &= ~(1 << PD7);
            }
            else
            {
                DDRD |= (1 << PD7);
            }
            break;
        case 2:
            if(in)
            {
                DDRC &= ~(1 << PC0);
            }
            else
            {
                DDRC |= (1 << PC0);
            }
            break;
    }
}

bool gpio_is_input(uint8_t gpio_num)
{
    switch(gpio_num)
    {
        case 0:
            return ((DDRB & (1 << PB2)) == 0);
            break;
        case 1:
            return ((DDRD & (1 << PD7)) == 0);
            break;
        case 2:
            return ((DDRC & (1 << PC0)) == 0);
            break;
    }
    return false;
}

void set_gpio_value(uint8_t gpio_num, bool hi)
{
    switch(gpio_num)
    {
        case 0:
            if(hi)
            {
                PORTB |= (1 << PB2);
            }
            else
            {
                PORTB &= ~(1 << PB2);
            }
            break;
        case 1:
            if(hi)
            {
                PORTD |= (1 << PD7);
            }
            else
            {
                PORTD &= ~(1 << PD7);
            }
            break;
        case 2:
            if(hi)
            {
                PORTC |= (1 << PC0);
            }
            else
            {
                PORTC &= ~(1 << PC0);
            }
            break;
    }
}

bool get_gpio_value(uint8_t gpio_num)
{
    switch(gpio_num)
    {
        case 0:
            return ((PINB & (1 << PB2)) != 0);
            break;
        case 1:
            return ((PIND & (1 << PD7)) != 0);
            break;
        case 2:
            return ((PINC & (1 << PC0)) != 0);
            break;
    }
    return false;
}
