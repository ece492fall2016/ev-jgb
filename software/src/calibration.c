#include "calibration.h"

uint16_t get_calibrated_value(uint16_t value, uint8_t calibration_setting)
{
    uint16_t cal_value = value;
    uint32_t new_val = (uint32_t) cal_value;

    switch(calibration_setting)
    {
        case GENERIC_CAL:
            return cal_value;
            break;
        case THROTTLE_OUT_CAL:
            new_val = new_val * 0x0000f800;
            new_val = new_val / 0x0000ff00;
            cal_value = (uint16_t) new_val;
            //if(cal_value > 0xcd00) cal_value = 0xcd00;
            break;
        case LOAD_OUT_CAL:
            new_val = new_val * 0x0000cd00;
            new_val = new_val / 0x0000ffff;
            cal_value = (uint16_t) new_val;
            //if(cal_value > 0xcd00) cal_value = 0xcd00;
            break;
    }
    return cal_value;
}
